import React, { Component } from 'react';
import Grid from '@material-ui/core/grid';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/icon';
import CreditIcon from "@material-ui/icons/MonetizationOn";
const styles = theme => ({
  pic:{
    width:'300px',
    height:'auto',
  },
  gridContainer:{
    margin:'50px auto auto auto'
  },
  Callsign:{
    fontWeight:'900'
  },
  creditIcon:{
    color:'#03A9F4',
    top:'10px',
    position:'relative',
    top:'7px'
  },
  ShipStats:{
    marginTop:'20px',
    width:'80%'
  }
})

class ShipBar extends Component {

  render() {
    return (
      <div className="Ship" >
        <Grid container spacing={0} alignItems="center" direction="column" justify="center" className={this.props.classes.gridContainer}>
          <Grid item xs>
            <img src={"https://i.imgur.com/XNZjoUT.jpg"} className={this.props.classes.pic} alt="UserPic"/>
          </Grid>
          <Grid item xs>
            <Typography variant="h5" className={this.props.classes.Callsign}>
              SPD-1000
            </Typography>

          </Grid>
          <Grid item xs>
            <Typography variant="h6" >
              Speedy
            </Typography>
          </Grid>
          <Grid item xs className={this.props.classes.ShipStats}>
            <Typography variant="h6">
              Inventory: 150kg/500kg
            </Typography>
            <Typography variant="h6">
              Speed: 2
            </Typography>
            <Typography variant="h6">
              Level: 9
            </Typography>
          </Grid>
        </Grid>
      </div>
    );
  }

}

export default withStyles(styles)(ShipBar);
