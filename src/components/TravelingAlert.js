import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {withStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  DialogText:{
    fontSize:'25px',
    color:'black',
    margin:'15px'
  },
  Button: {
    margin:'5px 25px 5px 0px',
    borderStyle: 'solid',
    borderWidth:'1px',
    borderColor:'#03A9F4',
    borderRadius: 15,
    letterSpacing: 1,
    fontWeight: "normal",
    color: "white",
    backgroundColor: "#03A9F4",
    "&:hover": {
        backgroundColor: "#E0E0E0",
        borderColor:'#03A9F4',
        color:'#03A9F4'
      }
  },
  SecondaryButton:{
    margin:'5px 5px 5px 25px',
    borderStyle: 'solid',
    borderWidth:'1px',
    borderColor:'#03A9F4',
    borderRadius: 15,
    letterSpacing: 1,
    fontWeight: "normal",
    color: "black",
    backgroundColor: "#E0E0E0",
    "&:hover": {
        backgroundColor: "#E0E0E0",
        borderColor:'#03A9F4',
        color:'#03A9F4'
      }
  }
})

const DateFormatOptions = { year: 'numeric', month: 'long', day: 'numeric', hour:'2-digit', minute:'2-digit', second:'2-digit' };
class TravelingAlert extends React.Component {
  constructor(props) {
    super(props);
    console.log(props)
    this.state = {
      open: true
    };

    this.time = new Date(this.props.user.ArrivalTimeToStation)

  }


  render(){
      return(
        <div>
          <Dialog
            open={this.state.open}

            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogContent style={{backgroundColor:'#E0E0E0'}}>
              <DialogContentText id="alert-dialog-description" className={this.props.classes.DialogText}>
                    You are traveling to the {this.props.user.Destination.Name}. Arrival time: {this.time.toLocaleString('en-GB', DateFormatOptions)}
              </DialogContentText>
            </DialogContent>
          </Dialog>
        </div>
    );
  }
}

export default withStyles(styles)(TravelingAlert)
