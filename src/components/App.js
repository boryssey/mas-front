import React, {Component} from 'react';
import UserBar from './UserBar';
import ShipBar from './ShipBar'
import Header from './Header'
import StationTable from './StationTable'
import "../styles/MainContainer.css"
class App extends Component {
    render() {
        return (
            <div className="MainContainer">
                <div className="LeftBar">
                  <UserBar/>
                  <ShipBar/>
                </div>
                <div className="content">
                  <Header/>
                  <StationTable/>
                </div>
            </div>
        );
    }
}

export default App;
