import React, { Component } from 'react';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root:{
    flexGrow: 1,
  },
  AppBar:{
    backgroundColor:'#E0E0E0',
    boxShadow:'none',
    borderBottom:'2px solid #03A9F4'
  }
})

class Header extends Component {


  render() {
    return (
      <div className={this.props.classes.root}>
      <AppBar position="static" color="default" className={this.props.classes.AppBar}>
        <Toolbar>
          <Typography variant="h6" color="inherit">
            List of Stations
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
    );
  }

}

export default withStyles(styles)(Header);
