import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import RingLoader from 'react-spinners/RingLoader';
import TimeToTravelDialog from './TimeToTravelDialog'
import TravelingAlert from './TravelingAlert';
import ErrorAlert from './ErrorAlert';
const styles = theme => ({
  root: {
    minWidth:'600px',
    width:'auto',
    margin:'5px',
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    overflowX: 'auto',
  },
  table: {
    borderColor:'black'
  },
  row:{
    borderColor:'black'
  },
  cell:{
    borderBottom: '0.5px solid #AEAEAE',
  }
});



class StationTable extends Component{
  constructor(props) {
    super(props);
    this.state = {
      data : [{
        StationId:1,
        Name:'',
        PlanetSystem:{
          Name:''
        },
        Affiliation:'',
        DistanceTo:''
      }],
      user:{isTraveling:false},
      loading:true,
      TimeToTravelDialogOpen:false,
      selectedStation:{},
      timeToTravelToStation:0,
      error:false,
      errorText:''
    };

    this.handleClick = this.handleClick.bind(this);
    this.closeTimeToTravelDialog = this.closeTimeToTravelDialog.bind(this);
    this.loadData = this.loadData.bind(this);

  }

  handleClick(event, id){
    var station = this.state.data.find(d=>d.StationId === id);
    if(station.Affiliation === 'FederationStation'&& this.state.user.Faction === 'Pirate'){
      this.setState({
        error:true,
        errorText:"You cannot travel to this station. It's too risky"
      })
      return;
    }
    if(station.StationId === this.state.user.CurrentLocation.StationId){
      this.setState({
        error:true,
        errorText:'You are already located at this station'
      })
      return;
    }
    fetch('https://localhost:44368/api/Travel?userId=2&stationId='+id).then(d=> d.json()).then((d)=>{
      this.setState({
        TimeToTravelDialogOpen:true,
        selectedStation:station,
        timeToTravelToStation:d
      })
    });
  }

  closeTimeToTravelDialog(){
    this.setState({
      TimeToTravelDialogOpen:false
    })
    this.loadData();
  }




  loadData(){
    fetch('https://localhost:44368/api/Stations?userId=2').then(d => d.json()).then(d=> this.setState({
      data:d,
      loading:false
    }))
    fetch('https://localhost:44368/api/users/2').then(d => d.json()).then(d=> this.setState({
      user:d,
    }))

  }
  closeError = ()=>{
    this.setState({
      error:false,
      errorText:''
    })
  }
  render(){

    return (

      <div className = "Table" style={{display:'grid'}}>
        {this.state.loading ? <RingLoader
          css={{margin:'auto',marginTop:'100px'}}
          sizeUnit={"px"}
          size={150}
          color={'#03A9F4'}
          loading={this.state.loading}
        /> :
        <Table className={this.props.classes.root}>
          <TableHead className={this.props.classes.table}>
            <TableRow className={this.props.classes.table}>
              <TableCell align="left" className={this.props.classes.cell}>Station Id</TableCell>
              <TableCell className={this.props.classes.cell}>Station name</TableCell>
              <TableCell align="right" className={this.props.classes.cell}>Planet System</TableCell>
              <TableCell align="right" className={this.props.classes.cell}>Affiliation</TableCell>
              <TableCell align="right" className={this.props.classes.cell}>Distance</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.data.map(row => (
              <TableRow key={row.StationId} className={this.props.classes.cell} onClick={a=>this.handleClick(a, row.StationId)}>
                <TableCell align="left" className={this.props.classes.cell}>{row.StationId}</TableCell>
                <TableCell component="th" scope="row" className={this.props.classes.cell}>
                  {row.Name}
                </TableCell>
                <TableCell align="right" className={this.props.classes.cell}>{row.PlanetSystem.Name}</TableCell>
                <TableCell align="right" className={this.props.classes.cell}>{row.Affiliation}</TableCell>
                <TableCell align="right" className={this.props.classes.cell}>{row.DistanceTo} l.y.</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        }
        {this.state.error ? <ErrorAlert open={this.state.error} closeAction={this.closeError}>{this.state.errorText}</ErrorAlert> : null}
        {this.state.user.isTraveling ? <TravelingAlert loadData={this.loadData()}  user={this.state.user} closeAction={this.handleCloseTraveling}/> : null}
        {this.state.TimeToTravelDialogOpen ? <TimeToTravelDialog closeAction = {this.closeTimeToTravelDialog} selectedStation={this.state.selectedStation} timeToTravelToStation={this.state.timeToTravelToStation} /> : null }
        </div>
    );
  }
  componentDidMount(){
    this.loadData();
  }
}

export default withStyles(styles)(StationTable);
