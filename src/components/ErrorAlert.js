import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {withStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const styles = theme => ({
  DialogText:{
    fontSize:'25px',
    color:'black',
    margin:'25px',
    marginBottom:'10px'

  },
  Button: {
    margin:'5px 5px 5px auto',
    borderStyle: 'solid',
    borderWidth:'1px',
    borderColor:'#03A9F4',
    borderRadius: 15,
    letterSpacing: 1,
    fontWeight: "normal",
    color: "white",
    backgroundColor: "#03A9F4",
    "&:hover": {
        backgroundColor: "#E0E0E0",
        borderColor:'#03A9F4',
        color:'#03A9F4'
      }
  },
})
class ErrorAlert extends React.Component {
  constructor(props) {
    super(props);
    console.log(props)
    this.state = {
      open: this.props.open
    };
    this.handleClose = this.handleClose.bind(this);

  }

  handleClose() {
    this.setState({
      open:false
    })
    this.props.closeAction();
  }

  render(){
      return(
        <div>
          <Dialog
            open={this.state.open}
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"

          >
            <DialogContent style={{backgroundColor:'#E0E0E0'}}>
              <DialogContentText id="alert-dialog-description" className={this.props.classes.DialogText}>
                    {this.props.children}
              </DialogContentText>
            </DialogContent>
            <DialogActions style={{backgroundColor:'#E0E0E0', justifyContent:'space-between'}}>
              <Button onClick={this.handleClose} className={this.props.classes.Button}>
                Ok
              </Button>
            </DialogActions>
          </Dialog>
        </div>
    );
  }
}

export default withStyles(styles)(ErrorAlert)
