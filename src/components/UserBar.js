import React, { Component } from 'react';
import Grid from '@material-ui/core/grid';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/icon';
import CreditIcon from "@material-ui/icons/MonetizationOn";
const styles = theme => ({
  pic:{
    width:'250px',
    height:'auto',

  },
  gridContainer:{
    margin:'auto',
  },
  username:{
    fontWeight:'900'
  },
  creditIcon:{
    color:'#03A9F4',
    top:'10px',
    position:'relative',
    top:'7px'
  }
})

class UserBar extends Component {

  render() {
    return (
      <div className="User">
        <Grid container spacing={0} alignItems="center" direction="column" justify="center" className={this.props.classes.gridContainer}>
          <Grid item xs>
            <img src={"https://i.imgur.com/pg1qEmB.jpg"} className={this.props.classes.pic} alt="UserPic"/>
          </Grid>
          <Grid item xs>
            <Typography variant="h5" className={this.props.classes.username}>
              BORYSSEY
            </Typography>

          </Grid>
          <Grid item xs>
            <Typography variant="h6" >
              Pirate
            </Typography>
          </Grid>
          <Grid item xs>
            <Typography variant="h6">
              Credits: 1500 <CreditIcon className={this.props.classes.creditIcon} fontSize="large"/>
            </Typography>
          </Grid>
        </Grid>
      </div>
    );
  }

}

export default withStyles(styles)(UserBar);
